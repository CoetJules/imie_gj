package Math;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class AdditionTest {
    protected Addition op;
    protected int a;
    protected int b;
    protected int res = a + b;

    @Before
    public void setUp() {
        op = new Addition();

    }

    @After
    public void tearDown() {
    }

    @Test
    public void testCalculerEntier() throws Exception {
        a = 2;
        b = 2;
        res = a + b;
        assertTrue("", op.calculer(a, b) == res);
    }
}

       /* a = -1;
        b = 2;
        res = a + b;
        assertTrue("", op.calculer(a, b) == res);

        a = -2;
        b = -1;
        res = a + b;
        assertTrue("", op.calculer(a, b) == res);

        a = 0;
        b = 2;
        res = a + b;
        assertTrue("", op.calculer(a, b) == res);

        a = 0;
        b = -1;
        res = a + b;
        assertTrue("", op.calculer(a, b) == res);

        a = 2;
        b = 0;
        res = a + b;
        assertTrue("", op.calculer(a, b) == res);

        a = -1;
        b = 0;
        res = a + b;
        assertTrue("", op.calculer(a, b) == res);

        a = -1;
        b = -1;
        res = a + b;
        assertTrue("", op.calculer(a, b) == res);

        a = Integer.MAX_VALUE;
        b = Integer.MAX_VALUE;
        res = a + b;
        assertFalse("", op.calculer(a, b) != res);

        a = Integer.MAX_VALUE;
        b = 0;
        res = a + b;
        assertFalse("", op.calculer(a, b) != res);

        a = -1;
        b = Integer.MAX_VALUE;
        res = a + b;
        assertFalse("", op.calculer(a, b) != res);
    }
}*/
