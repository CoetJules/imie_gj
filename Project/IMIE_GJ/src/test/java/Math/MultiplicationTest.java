package Math;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class MultiplicationTest {

    protected Multiplication op;
    protected int a;
    protected int b;
    protected int res = a+b;

    @Before
    public void setUp() {
        op = new Multiplication();
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testCalculer() throws Exception {
        a=2;
        b=2;
        res = a*b;
        assertTrue("", op.calculer(a,b) == res);

        a=-1;
        b=2;
        res = a*b;
        assertTrue("", op.calculer(a,b) == res);

        a=-2;
        b=-1;
        res = a*b;
        assertTrue("", op.calculer(a,b) == res);

        a=0;
        b=2;
        res = a*b;
        assertTrue("", op.calculer(a,b) == res);

        a=0;
        b=-1;
        res = a*b;
        assertTrue("", op.calculer(a,b) == res);

        a=2;
        b=0;
        res = a*b;
        assertTrue("", op.calculer(a,b) == res);

        a=-1;
        b=0;
        res = a*b;
        assertTrue("", op.calculer(a,b) == res);

        a=-1;
        b=-1;
        res = a*b;
        assertTrue("", op.calculer(a,b) == res);

        a= Integer.MAX_VALUE;
        b= Integer.MAX_VALUE;
        res=a*b;
        assertFalse("",op.calculer(a,b) != res);

        a= Integer.MAX_VALUE;
        b= 0;
        res=a*b;
        assertFalse("",op.calculer(a,b) != res);

        a= -1;
        b= Integer.MAX_VALUE;
        res=a*b;
        assertFalse("",op.calculer(a,b) != res);
    }

}
